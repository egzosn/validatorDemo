package com.egzosn.validator.demo.controller;

import com.egzosn.validator.error.MsgCode;
import com.egzosn.validator.validator.annotation.Validated;
import com.egzosn.validator.validator.constraints.QQ;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by ZaoSheng on 2015/7/2.
 */
@RestController
@RequestMapping("vali")
@Validated
public class User1Controller extends SupportController {

    @RequestMapping("qq")
    public Map<String, Object> qq(@MsgCode(1003) @QQ String qq) {
        System.out.println(qq);
        return successData();
    }

}
