package com.egzosn.validator.demo.service;



import com.egzosn.validator.exception.CommonException;

import java.util.UUID;

/**
 * Created by egan on 2014/11/6.
 */
public abstract class SupportService {


    protected void throwError(Integer code, String message) {
        throw new CommonException(code, message);
    }

    protected void throwError(Integer code) {
        throwError(code, "");
    }

    public String getUUID(){
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

}
