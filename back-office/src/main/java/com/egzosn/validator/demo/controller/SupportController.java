package com.egzosn.validator.demo.controller;

import com.egzosn.validator.validator.handler.ExceptionInterceptor;

import java.util.*;

public abstract class SupportController {


    protected Map<String, Object> successData() {
        Map<String, Object> data = newData();
        data.put(ExceptionInterceptor.CODE_KEY, 0);
        return data;
    }

    /**
     * @return
     */
    protected Map<String, Object> successData(String key, Object result) {
        Map<String, Object> data = newData();
        data.put(ExceptionInterceptor.CODE_KEY, 0);
        data.put(key, result);
        return data;
    }


    protected Map<String, Object> newData() {
        Map<String, Object> data = new HashMap<String, Object>();
        return data;
    }

/*    @ExceptionHandler(Exception.class)
    @ResponseBody
    public void handleException(Exception e) throws Exception {
      throw e;
    }*/


}
