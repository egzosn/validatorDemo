package com.egzosn.validator.demo.entity;


import com.egzosn.validator.error.MsgCode;
import com.egzosn.validator.validator.constraints.QQ;
import com.egzosn.validator.validator.constraints.RepositoryData;
import com.egzosn.validator.validator.plug.Restriction;
import com.egzosn.validator.demo.service.UserService;

/**
 * Created by ZaoSheng on 2015/7/2.
 */
public class User {


//    @MsgCode(1005)
    @RepositoryData(clazz = UserService.class, method = "isExistEmail", restriction = Restriction.EQ, expect = "true", message = "1005")
    private String email;

    @MsgCode(1004)
    @QQ
    private String qq;
//    @IdCard
    private String idCard;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
}
