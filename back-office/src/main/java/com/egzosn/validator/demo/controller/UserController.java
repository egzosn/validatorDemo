package com.egzosn.validator.demo.controller;

import com.egzosn.validator.validator.annotation.Validated;
import com.egzosn.validator.demo.entity.User;
import com.egzosn.validator.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

/**
 * Created by ZaoSheng on 2015/7/2.
 */
@RestController
@RequestMapping("user")
@Validated
public class UserController extends SupportController {
    @Autowired
    UserService userService;

    @RequestMapping("/login")
    public Map<String, Object> login(@Valid User user) {

        return successData();
    }


    /*
    @Valid
    @Deprecated
    @RequestMapping("set")
    public Map<String, Object> set(@QQ String qq) {
        System.out.println(qq);
        return successData();
    }*/

}
