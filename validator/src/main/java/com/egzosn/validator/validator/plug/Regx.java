package com.egzosn.validator.validator.plug;

import org.springframework.util.StringUtils;

/**
 * Created by ZaoSheng on 2015/5/12.
 */
public final class Regx {
	public final static String PHONE = "^1(3|4|5|6|7|8)\\d{9}$";
    public final static String TELEPHONE = "^0\\d{2,3}\\+?\\-?\\d{7,8}$";
    public final static String QQ = "^[1-9][0-9]{4,9}$";
    public final static String ZIPCODE = "^[1-9]\\d{5}$";
    public final static String SAFEPASS = "^(([A-Z]*|[a-z]*|\\d*|[-_\\~!@#\\$%\\^&\\*\\.\\(\\)\\[\\]\\{\\}<>\\?\\\\\\/\\'\\\"]*)|.{0,5})$|\\s";
    public final static String PASSWORD = "^(?!_)(?!.*?_$)[a-zA-Z0-9_]{%d,%d}$";
    public final static String CHINESE = "^[\\u4E00-\\u9FA5]{%d,%d}$";
    public final static String LOGINNAME = "^[\\u0391-\\uFFE5\\w]+$";
    public final static String DECIMAL = "([1-9][0-9]{1,%d}|0)(\\.[\\d]{0,%d})?";

    public final static boolean isDigits(String str) {
        if(StringUtils.isEmpty(str)) {
            return false;
        } else {
            for(int i = 0; i < str.length(); ++i) {
                if(!Character.isDigit(str.charAt(i))) {
                    return false;
                }
            }

            return true;
        }
    }

    public final static boolean isNumber(String str) {
        if(StringUtils.isEmpty(str)) {
            return false;
        } else {
            char[] chars = str.toCharArray();
            int sz = chars.length;
            boolean hasExp = false;
            boolean hasDecPoint = false;
            boolean allowSigns = false;
            boolean foundDigit = false;
            int start = chars[0] == 45?1:0;
            int i;
            if(sz > start + 1 && chars[start] == 48 && chars[start + 1] == 120) {
                i = start + 2;
                if(i == sz) {
                    return false;
                } else {
                    while(i < chars.length) {
                        if((chars[i] < 48 || chars[i] > 57) && (chars[i] < 97 || chars[i] > 102) && (chars[i] < 65 || chars[i] > 70)) {
                            return false;
                        }

                        ++i;
                    }

                    return true;
                }
            } else {
                --sz;

                for(i = start; i < sz || i < sz + 1 && allowSigns && !foundDigit; ++i) {
                    if(chars[i] >= 48 && chars[i] <= 57) {
                        foundDigit = true;
                        allowSigns = false;
                    } else if(chars[i] == 46) {
                        if(hasDecPoint || hasExp) {
                            return false;
                        }

                        hasDecPoint = true;
                    } else if(chars[i] != 101 && chars[i] != 69) {
                        if(chars[i] != 43 && chars[i] != 45) {
                            return false;
                        }

                        if(!allowSigns) {
                            return false;
                        }

                        allowSigns = false;
                        foundDigit = false;
                    } else {
                        if(hasExp) {
                            return false;
                        }

                        if(!foundDigit) {
                            return false;
                        }

                        hasExp = true;
                        allowSigns = true;
                    }
                }

                return i < chars.length?(chars[i] >= 48 && chars[i] <= 57?true:(chars[i] != 101 && chars[i] != 69?(!allowSigns && (chars[i] == 100 || chars[i] == 68 || chars[i] == 102 || chars[i] == 70)?foundDigit:(chars[i] != 108 && chars[i] != 76?false:foundDigit && !hasExp)):false)):!allowSigns && foundDigit;
            }
        }
    }
}
